#!/usr/bin/env node

let caller = "this package"
try {
  caller = require(`${process.cwd()}/package.json`).name
} catch (err) {}
const donationLink = require(`${__dirname}/package.json`).donate.url

console.log(`
    Thanks for installing ${caller}!

    If you like this package, be sure to star its repo,
    and please consider donating:

        ${donationLink}
`)
